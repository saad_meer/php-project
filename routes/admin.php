<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes([
  'register' => false, // Registration Routes...
  'reset' => false, // Password Reset Routes...
  'verify' => false, // Email Verification Routes...
]);
Route::get('/admin/login','Auth\LoginController@login');

Route::post('/admin/login','Auth\LoginController@loginProcess');
Route::post('/admin/logout','Auth\LoginController@logout');

// Route::get('/register','Auth\LoginController@signup');
// Route::post('/register','Auth\LoginController@signupProcess');

Route::get('/admin/dashboard', 'HomeController@index')->middleware('auth');
Route::get('refresh-csrf', function(){
    return csrf_token();
});
Route::group(['middleware' => 'auth','namespace' => 'Admin','prefix' => 'admin'], function () {

    Route::post('/getsubcategory','CategoryController@getsubcategory');
    Route::post('/getsinglesubcategory','CategoryController@getsinglesubcategory');

    Route::group(['prefix' => 'users',], function () {
        Route::get('/editprofile', 'UsersController@profile');
        Route::post('/editprofileProcess', 'UsersController@profileProcess');

        Route::get('/display', 'UsersController@index');
        Route::post('/display', 'UsersController@display');
        Route::get('/add', 'UsersController@add');
        Route::post('/addProcess', 'UsersController@addProcess');
        Route::get('/edit/{id}', 'UsersController@edit');
        Route::post('/editProcess', 'UsersController@editProcess');
        Route::get('/delete/{id}', 'UsersController@delete');
    });

    Route::group(['prefix' => 'category',], function () {
        Route::get('/display', 'CategoryController@index');
        Route::post('/display', 'CategoryController@display');
        Route::get('/add', 'CategoryController@add');
        Route::post('/addProcess', 'CategoryController@addProcess');
        Route::get('/edit/{id}', 'CategoryController@edit');
        Route::post('/editProcess', 'CategoryController@editProcess');
        Route::get('/delete/{id}', 'CategoryController@delete');
    });
    Route::group(['prefix' => 'sub-category',], function () {
        Route::get('/display', 'SubCategoryController@index');
        Route::post('/display', 'SubCategoryController@display');
        Route::get('/add', 'SubCategoryController@add');
        Route::post('/addProcess', 'SubCategoryController@addProcess');
        Route::get('/edit/{id}', 'SubCategoryController@edit');
        Route::post('/editProcess', 'SubCategoryController@editProcess');
        Route::get('/delete/{id}', 'SubCategoryController@delete');
    });
    Route::group(['prefix' => 'author',], function () {
        Route::get('/display', 'AuthorController@index');
        Route::post('/display', 'AuthorController@display');
        Route::get('/add', 'AuthorController@add');
        Route::post('/addProcess', 'AuthorController@addProcess');
        Route::get('/edit/{id}', 'AuthorController@edit');
        Route::post('/editProcess', 'AuthorController@editProcess');
        Route::get('/delete/{id}', 'AuthorController@delete');
    });
    Route::group(['prefix' => 'quote_template',], function () {
        Route::get('/display', 'QuotoTemplateController@index');
        Route::post('/display', 'QuotoTemplateController@display');
        Route::get('/add', 'QuotoTemplateController@add');
        Route::post('/addProcess', 'QuotoTemplateController@addProcess');
        Route::get('/edit/{id}', 'QuotoTemplateController@edit');
        Route::post('/editProcess', 'QuotoTemplateController@editProcess');
        Route::get('/delete/{id}', 'QuotoTemplateController@delete');
    });
    Route::group(['prefix' => 'quotes',], function () {
        Route::get('/display', 'QuoteController@index');
        Route::post('/display', 'QuoteController@display');
        Route::get('/add', 'QuoteController@add');
        Route::post('/addProcess', 'QuoteController@addProcess');
        Route::get('/edit/{id}', 'QuoteController@edit');
        Route::post('/editProcess', 'QuoteController@editProcess');
        Route::get('/delete/{id}', 'QuoteController@delete');
    });
    Route::group(['prefix' => 'pages',], function () {
        Route::get('/display', 'PagesController@index');
        Route::post('/display', 'PagesController@display');
        Route::get('/add', 'PagesController@add');
        Route::post('/addProcess', 'PagesController@addProcess');
        Route::get('/edit/{id}', 'PagesController@edit');
        Route::post('/editProcess', 'PagesController@editProcess');
        Route::get('/delete/{id}', 'PagesController@delete');
    });
    Route::group(['prefix' => 'blog',], function () {
        Route::get('/display', 'BlogController@index');
        Route::post('/display', 'BlogController@display');
        Route::get('/add', 'BlogController@add');
        Route::post('/addProcess', 'BlogController@addProcess');
        Route::get('/edit/{id}', 'BlogController@edit');
        Route::post('/editProcess', 'BlogController@editProcess');
        Route::get('/delete/{id}', 'BlogController@delete');
    });

});
