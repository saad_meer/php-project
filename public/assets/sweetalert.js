function swalsuccess(msg="",redirect_url="")
{
    var url = '{{url("/")}}';

    swal({
        title: msg,
        icon: "success",
        ClassName:"swal-title",
        showCancelButton: false,
        showConfirmButton: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            window.location.href  = redirect_url
        } else {

        }
      });

}
function swalsuccesscurrent(msg="")
{
    var url = '{{url("/")}}';

    swal({
        title: msg,
        icon: "success",
        ClassName:"swal-title",
        showCancelButton: false,
        showConfirmButton: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            window.location.reload(true);
        } else {

        }
      });

}
function swalerror(msg="")
{
    swal({
        title: msg,
        icon: "error",
        ClassName:"swal-title"
      });
}
function swalwarning(msg="")
{
    swal({
        title: msg,
        icon: "warning",
        ClassName:"swal-title",
        showCancelButton: false,
        showConfirmButton: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            return true;
        } else {
            return false;

        }
      });
}
