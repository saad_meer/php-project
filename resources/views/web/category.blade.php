@extends('web.layouts.header')
@section('content')

  <div class="container main-container container-xxl my-5">
    <div class="row gy-5">
      <div class="col-lg-8 col-xxl-9">
         <!-- ========================= Quotes-card section ========================== -->
        <div class="row gy-4 quotes-card" data-masonry='{"percentPosition": true }'>

          @if(isset($result['categoryArray']) && count($result['categoryArray'])>0)
            @foreach ($result['categoryArray'] as $key=>$category)
               @include('web.category-template',$category)
            @endforeach
          @endif
        </div>
      </div>
       <!-- ========================= Right-Side-Section ========================== -->
      <div class="col-lg-4 col-xxl-3 ps-xl-5  d-none d-lg-block">
        <div class="card ">
          <div class="card-body">
             <div class="d-flex">
              <h2 class="card-title right-side-card-heading ">
                <i class="fas fa-birthday-cake text-primary"></i> &nbsp;
                <span class="m-auto">Today's Birthdays</span></h2>

             </div>
              <hr>
              <!--Accordion wrapper-->
              <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
                @include('web.birthday')
                <!-- Accordion card -->

              </div>
              <!-- Accordion wrapper -->
          </div>
        </div>
          <div class="card mt-5">
            <div class="card-body">
              <h2 class="card-title text-left right-side-card-heading  ">
                <i class="fas fa-boxes text-primary"></i> &nbsp;
                 <span class="m-auto">Categories</span></h2>
                <hr>
                <!--Accordion wrapper-->
                <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
                @if(isset($result['categoryArray']) && count($result['categoryArray'])>0)
                  @foreach ($result['categoryArray'] as $key=>$value)
                  <!-- Accordion card -->
                  <div class="card border-0">

                    <!-- Card header -->
                    <div class="card-header" role="tab" id="heading_{{$key}}">
                      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapse_{{$key}}"
                        aria-expanded="false" aria-controls="collapse_{{$key}}">
                        <h5 class="m-0 toggle-heading">
                          <i style="color:#ff5d5d;"  class="fas fa-angle-right rotate-icon"></i> &nbsp; <a style="color:inherit;" href="{{url($value->category_slug)}}">{{$value->category_name}}</a>
                        </h5>
                      </a>
                    </div>
                    @if(isset($value->subcategory))
                    <!-- Card body -->
                    <div id="collapse_{{$key}}" class="collapse" role="tabpanel" aria-labelledby="heading_{{$key}}"
                      data-parent="#accordionEx">
                      <div class="card-body py-0 ">
                        <ul class="list-unstyled list-group-flush">
                            @foreach ($value['subcategory'] as $keys=>$values)
                            <li class="list-group-item"><a style="color:inherit;"  href="{{url($values->sub_category_slug)}}">{{$values->subcategory_name}}</a></li>
                            @endforeach

                        </ul>
                      </div>
                    </div>
                    @endif
                  </div>
                  @endforeach
                  <!-- Accordion card -->
                @endif

                </div>
                <!-- Accordion wrapper -->
            </div>
          </div>

      </div>
    </div>
  </div>
@endsection





