@php $birthday=birthday();@endphp
@if(isset($birthday) && count($birthday))
@foreach ($birthday as $birthday_value)
    <div class="card border-0">

    <!-- Card header -->
    <div class="card-header" role="tab" id="headingOne1">
        <a class="collapsed d-flex" data-toggle="collapse" data-parent="#accordionEx" href="#collapsecat1"
        aria-expanded="false" aria-controls="collapsecat1" class="d-flex ">
        <a  class="collapsed d-flex" href="{{url('authors/'.$birthday_value->author_first_name.'-'.$birthday_value->author_last_name)}}">
        <h5 class="m-0 toggle-heading">
            <i class="fas fa-angle-right rotate-icon"></i> &nbsp;{{$birthday_value->author_first_name.' '.$birthday_value->author_last_name}}
        </h5>
    </a>
        </a>
    </div>


    </div>
@endforeach
@else
<h5 class="m-0 toggle-heading">
    No Birthday Today
</h5>
@endif
