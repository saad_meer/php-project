
@extends('layouts.header')
@section('title', 'Edit Category')
@section('content')


    <div class="content-body" >
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Edit Categories</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/category/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div data-label="Category" class="df-example demo-forms">
                        <form id="editcategory" action="{{url('admin/category/editProcess')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="category_name" value="{{$category->category_name}}" placeholder="Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Meta</label>
                                    <input type="text" class="form-control" name="category_meta" value="{{$category->category_meta}}" placeholder="Meta">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>MetaData</label>
                                    <input type="text" class="form-control" name="category_meta_data" value="{{$category->category_meta_data}}" placeholder="MetaData">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                        <select class="custom-select" name="category_status">
                                            <option value="Active" @if($category->category_status=='Active') selected @endif>Active</option>
                                            <option value="Inactive" @if($category->category_status=='Inactive') selected @endif>Inactive</option>
                                        </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="category_image" id="customFile">
                                </div>

                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                    </div><!-- df-example -->
                </div>
                <div class="col-sm-12 mt-4">
                    @if(!empty($category->category_image))
                    <div data-label="Preview" class="df-example demo-forms">
                        <div class="form-row">
                            <img src="{{url('storage/'.$category->category_image)}}" class="rounded float-left w-25" alt="">
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')

<script type="text/javascript">


$('#editcategory').submit(function(event) {
    var url = '{{url("/")}}';
//prevent the form from submitting by default
event.preventDefault();

var frm = $('#editcategory');

var formData = new FormData($(this)[0]);
formData.append('category_id','<?php echo $category->category_id; ?>');
$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            swalsuccess(data.message,url+'/admin/category/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



