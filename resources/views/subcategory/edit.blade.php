
@extends('layouts.header')
@section('title','Add Subcategory')
@section('content')
    <div class="content-body" style="overflow: hidden;">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Add Sub-Categories</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/sub-category/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Sub-Category" class="df-example demo-forms">
                        <form id="subcategory" action="{{url('admin/sub-category/editProcess')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Category<span class="text-danger">*</span></label>
                                   <select name="category_id" id="" class="form-control">
                                    <option value="">Please Select Category</option>
                                    @foreach ($category as $value)
                                    <option value="{{$value->category_id}}" @if($value->category_id==$sub_category->category_id) selected @endif>{{$value->category_name}}</option>
                                    @endforeach

                                   </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="{{$sub_category->subcategory_name}}" name="subcategory_name" placeholder="Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Meta</label>
                                    <input type="text" class="form-control" value="{{$sub_category->subcategory_meta}}"  name="subcategory_meta" placeholder="Meta">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>MetaData</label>
                                    <input type="text" class="form-control" value="{{$sub_category->subcategory_meta_data}}"  name="subcategory_meta_data" placeholder="MetaData">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                        <select class="custom-select" name="subcategory_status">
                                            <option value="Active" @if($sub_category->subcategory_status=='Active') selected @endif>Active</option>
                                            <option value="Inactive" @if($sub_category->subcategory_status=='Inactive') selected @endif>Inactive</option>
                                        </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="subcategory_image" id="customFile">
                                    </div>
                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                    </div><!-- df-example -->
                </div>
                <div class="col-sm-12 mt-4">
                    @if(!empty($sub_category->subcategory_image))
                    <div data-label="Preview" class="df-example demo-forms">
                        <div class="form-row">
                            <img src="{{url('storage/'.$sub_category->subcategory_image)}}" class="rounded float-left w-25" alt="">
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')

<script type="text/javascript">


$('#subcategory').submit(function(event) {
    var url = '{{url("/")}}';
//prevent the form from submitting by default
event.preventDefault();

var frm = $('#subcategory');

var formData = new FormData($(this)[0]);
formData.append('id','<?php echo $sub_category->id; ?>');
$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
           // $('#subcategory')[0].reset();
           swalsuccess(data.message,url+'/admin/sub-category/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



