
@extends('layouts.header')
@section('title','Edit Quote')
@section('datatables')
<link href="{{url('lib/select2/css/select2.min.css')}}" rel="stylesheet">

@endsection
@section('content')


    <div class="content-body" >
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Edit Quote</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/quotes/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="quoto" class="df-example demo-forms">

                        <form id="editquote"  action="{{url('admin/quotes/editProcess')}}" method="POST">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Category<span class="text-danger">*</span></label>
                                   <select name="quote_category[]" id="category_id" multiple="multiple"  class="form-control select2 category_id">
                                    @php $selectcat=explode(',',$quote->quote_category) @endphp
                                    <option value="">Please Select Category</option>
                                    @foreach ($category as $value)
                                    <option value="{{$value->category_id}}" @if(in_array($value->category_id,$selectcat)) selected @endif>{{$value->category_name}}</option>
                                    @endforeach

                                   </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Sub Category</label>
                                   <select name="quote_sub_category[]" id="subcategory"  multiple="multiple"  class="form-control select2">
                                    <option value="">Please Select Sub Category</option>
                                    @php $selectsubcat=explode(',',$quote->quote_sub_category) @endphp
                                    @foreach ($sub_category as $value)
                                        @if(!empty($selectcat) && in_array($value->category_id,$selectcat))
                                        {
                                            <option value="{{$value->id}}" @if(in_array($value->id,$selectsubcat)) selected @endif>{{$value->subcategory_name}}</option>
                                        }
                                        @elseif(!empty($selectcat) && in_array($value->category_id,$selectcat))
                                        {
                                            <option value="{{$value->id}}">{{$value->subcategory_name}}</option>
                                        }
                                        @else

                                        @endif
                                    @endforeach

                                   </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Author</label>
                                   <select name="author_id"  class="form-control select2">
                                    <option value="">Please Select Author</option>
                                    @foreach ($author as $value)
                                    <option value="{{$value->id}}" @if($value->id==$quote->author_id) selected @endif>{{$value->author_first_name .''. $value->author_last_name}}</option>
                                    @endforeach
                                   </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                        <select class="custom-select" name="quotes_status">
                                            <option value="Active" @if($value->quotes_status=='Active') selected @endif>Active</option>
                                            <option value="Inactive" @if($value->quotes_status=='Inactive') selected @endif>Inactive</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input quote_has_image" onchange="checked_box()" @if($quote->quote_has_image==1) checked="checked" @endif name="quote_has_image" id="customCheck1" value="1">
                                        <label class="custom-control-label" for="customCheck1">Check if You Want Custom Quoto Background</label>
                                      </div>
                                </div>
                                <div class="form-group col-md-6" id="template_id">
                                    <label for="inputEmail4">Template</label>
                                   <select name="template_id"  class="form-control select2">
                                    <option value="">Please Select Template</option>
                                    @foreach ($quoto_template as $value)
                                    <option value="{{$value->id}}"  @if($value->template_id==$quote->template_id) selected @endif>{{$value->template_name}}</option>
                                    @endforeach
                                   </select>
                                </div>
                                <input type="hidden" name="old_image" value="{{$quote->quote_image}}">
                                <div class="form-group col-md-6 d-none" id="bg_image">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="quote_image" id="customFile">
                                </div>
                                <div class="col-md-12">
                                    <label>Discription</label>
                                    <textarea id="summernote" name="quote_text">{!!$quote->quote_text!!}</textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                    </div><!-- df-example -->
                </div>
                <div class="col-sm-12 mt-4">
                    @if(!empty($quote->quote_image))
                    <div data-label="Preview" class="df-example demo-forms">
                        <div class="form-row">
                            <img src="{{url('storage/'.$quote->quote_image)}}" class="rounded float-left w-25" alt="">
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')
<script src="{{url('lib/select2/js/select2.min.js')}}"></script>

<script type="text/javascript">
$('.select2').select2();

$(".category_id").change(function() {
    var html='';
    var url = '{{url("/")}}';
    var sub_category = $('.category_id').val();
    $.ajax({
    url: url+'/admin/getsubcategory',
    type: 'POST',
    data: {
        subCategory: sub_category
    },
    success: function (data) {
        if(data.code==200)
        {
            html='<option value="">Please Select Sub Category</option>';
            $.each( data.result, function( key, value ) {
                html+='<option value="'+value.id+'">'+value.subcategory_name+'</option>';
            });
            console.log(html);
            $('#subcategory').html(html);
        }
        if(data.code==404)
        {
            html='<option value="">Please Select Sub Category</option>';
            $('#subcategory').html(html);
        }
    },
    error: function (error) {
       // swalerror("SomeThing Went Wrong");
    }
});
});
$( document ).ready(function() {
    checked_box();
});
function checked_box()
{
    if(document.getElementById('customCheck1').checked) {
    $('#template_id').addClass('d-none');
    $('#bg_image').removeClass('d-none');
    }
    else
    {
        $('#template_id').removeClass('d-none');
        $('#bg_image').addClass('d-none');
    }
}
$('#editquote').submit(function(event) {
    var url = '{{url("/")}}';
//prevent the form from submitting by default
event.preventDefault();

var frm = $('#editquote');

var formData = new FormData($(this)[0]);
formData.append('id','<?php echo $quote->id; ?>');
$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            swalsuccess(data.message,url+'/admin/quotes/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



