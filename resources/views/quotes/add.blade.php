
@extends('layouts.header')
@section('title','Add Quote')
@section('datatables')
<link href="{{url('lib/select2/css/select2.min.css')}}" rel="stylesheet">

@endsection
@section('content')


    <div class="content-body" >
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Add Quote</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/quotes/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="quoto" class="df-example demo-forms">
                        <form id="addquote" action="{{url('admin/quotes/addProcess')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Category<span class="text-danger">*</span></label>
                                   <select name="quote_category[]" id="category_id" multiple="multiple"  class="form-control select2 category_id">
                                    <option value="">Please Select Category</option>
                                    @foreach ($category as $value)
                                    <option value="{{$value->category_id}}">{{$value->category_name}}</option>
                                    @endforeach

                                   </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Sub Category</label>
                                   <select name="quote_sub_category[]" id="subcategory"  multiple="multiple"  class="form-control select2">
                                    <option value="">Please Select Sub Category</option>
                                   </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Author</label>
                                   <select name="author_id"  class="form-control select2">
                                    <option value="">Please Select Author</option>
                                    @foreach ($author as $value)
                                    <option value="{{$value->id}}">{{$value->author_first_name .''. $value->author_last_name}}</option>
                                    @endforeach
                                   </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                        <select class="custom-select" name="quotes_status">
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="quote_has_image" id="customCheck1" value="1">
                                        <label class="custom-control-label" for="customCheck1">Check if You Want Custom Quoto Background</label>
                                      </div>
                                </div>
                                <div class="form-group col-md-6" id="template_id">
                                    <label for="inputEmail4">Template</label>
                                   <select name="template_id"  class="form-control select2">
                                    <option value="">Please Select Template</option>
                                    @foreach ($quoto_template as $value)
                                    <option value="{{$value->id}}">{{$value->template_name}}</option>
                                    @endforeach
                                   </select>
                                </div>
                                <div class="form-group col-md-6 d-none" id="bg_image">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="quote_image" id="customFile">
                                </div>
                                <div class="col-md-12">
                                    <label>Discription</label>
                                    <textarea id="summernote" name="quote_text"></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')
<script src="{{url('lib/select2/js/select2.min.js')}}"></script>

<script type="text/javascript">
$('.select2').select2();

$(".category_id").change(function() {
    var html='';
    var url = '{{url("/")}}';
    var sub_category = $('.category_id').val();
    $.ajax({
    url: url+'/admin/getsubcategory',
    type: 'POST',
    data: {
        subCategory: sub_category
    },
    success: function (data) {
        if(data.code==200)
        {
            html='<option value="">Please Select Sub Category</option>';
            $.each( data.result, function( key, value ) {
                html+='<option value="'+value.id+'">'+value.subcategory_name+'</option>';
            });
            console.log(html);
            $('#subcategory').html(html);
        }
        if(data.code==404)
        {
            html='<option value="">Please Select Sub Category</option>';
            $('#subcategory').html(html);
        }
    },
    error: function (error) {
       // swalerror("SomeThing Went Wrong");
    }
});
});

$("#customCheck1").change(function() {
    if(this.checked) {
        $('#template_id').addClass('d-none');
        $('#bg_image').removeClass('d-none');
    }
    else
    {
        $('#template_id').removeClass('d-none');
        $('#bg_image').addClass('d-none');
    }
});
$('#addquote').submit(function(event) {
    var url = '{{url("/")}}';

//prevent the form from submitting by default
event.preventDefault();

var frm = $('#addquote');

var formData = new FormData($(this)[0]);

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            $('#addquote')[0].reset();
            swalsuccess(data.message,url+'/admin/quotes/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



