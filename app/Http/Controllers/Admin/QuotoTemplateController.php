<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Admin\QuotoTemplate;
use App\Models\Admin\FontStyle;
use File;

class QuotoTemplateController extends Controller
{
    public function index()
    {
        return view('quote_template.list');
    }
    public function display(Request $request)
    {
        $columns = array(
            0 =>'template_name',
            1 =>'template_text_size',
            2=> 'template_status',
            3=> 'created_at',
            4=> 'id',
        );

            $totalData = QuotoTemplate::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $quote = QuotoTemplate::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
            else {
            $search = $request->input('search.value');

            $quote =  QuotoTemplate::where('template_name','LIKE',"%{$search}%")
                        ->orWhere('template_text_size', 'LIKE',"%{$search}%")
                        ->orWhere('template_status', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            $totalFiltered = QuotoTemplate::where('template_name','LIKE',"%{$search}%")
                            ->orWhere('template_text_size', 'LIKE',"%{$search}%")
                            ->orWhere('template_status', 'LIKE',"%{$search}%")

                        ->count();
            }

            $data = array();
            if(!empty($quote))
            {

            foreach ($quote as $value)
            {
            $edit =  url('/admin/quote_template/edit',$value->id);

            $nestedData['name'] = $value->template_name;
            $nestedData['size'] = $value->template_text_size;
            $nestedData['status'] = $value->template_status;
            $nestedData['created_at'] = format_date_time($value->created_at);
            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="'.$edit.'"><i class="fas fa-edit"></i>  Edit</a>
              <a class="dropdown-item"onClick="deleterow('.$value->id.')"><i class="fas fa-trash"></i> Delete</a>
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        $font_style=FontStyle::where('id','>',0)->get();
        return view('quote_template.add',compact('font_style'));
    }
    public function addProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'template_name' => 'required',
        ],
        [
            'template_name.required' => 'Template Name is Required',
         ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            $quote= new QuotoTemplate();
            $quote->template_name=$request->template_name;
            $quote->template_text_color=$request->template_text_color;
            $quote->template_text_style=$request->template_text_style;
            $quote->template_text_size=$request->template_text_size;
            $quote->template_font_family=$request->template_font_family;
            $quote->template_has_image=$request->template_has_image;
            $quote->template_status=$request->template_status;
            if(isset($request->template_has_image) && $request->template_has_image==1)
            {
                if ($request->hasFile('template_image')) {
                    $fileName = time().'_'.$request->template_image->getClientOriginalName();
                    $filePath = $request->file('template_image')->storeAs('uploads/template', $fileName, 'public');
                    $quote->template_image = $filePath;
                }
            }
            else
            {
                $quote->template_background_color=$request->template_background_color;
            }

            $quote->save();
            return response()->json(['code'=>200,'message'=>'Record Added Successfully']);


        }
    }
    public function edit($id)
    {
        $quote=QuotoTemplate::where('id','=',$id)->first();
        if($quote)
        {
            $font_style=FontStyle::where('id','>',0)->get();
            return view('quote_template.edit',compact('quote','font_style'));
        }
        return redirect()->back();
    }
    public function editProcess(Request $request)
    {
        $quote=QuotoTemplate::where('id','=',$request->id)->first();
        if($quote)
        {
            $validator = Validator::make($request->all(), [
                'template_name' => 'required',
            ],
            [
                'template_name.required' => 'Template Name is Required',
             ]);
            if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            else
            {
                $quote->template_name=$request->template_name;
                $quote->template_text_color=$request->template_text_color;
                $quote->template_text_style=$request->template_text_style;
                $quote->template_text_size=$request->template_text_size;
                $quote->template_font_family=$request->template_font_family;
                $quote->template_has_image=$request->template_has_image;
                $quote->template_status=$request->template_status;
                if(isset($request->template_has_image) && $request->template_has_image==1)
                {
                    if ($request->hasFile('template_image')) {
                        File::delete(public_path('storage/'.$request->old_image));
                        $fileName = time().'_'.$request->template_image->getClientOriginalName();
                        $filePath = $request->file('template_image')->storeAs('uploads/template', $fileName, 'public');
                        $quote->template_image = $filePath;
                    }
                }
                else
                {
                    $quote->template_background_color=$request->template_background_color;
                }

                $quote->save();
                return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);
            }

        }
        return redirect('admin/quote_template/display');

    }
    public function delete($id)
    {
        $delete=QuotoTemplate::where('id','=',$id)->first();
        if($delete)
        {
            $delete->delete();
            return response()->json(['code'=>404,'message'=>'Record Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }
}
