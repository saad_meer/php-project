<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Admin\Author;
use File;
class AuthorController extends Controller
{
    public function index()
    {
        return view('author.list');
    }
    public function display(Request $request)
    {
        $columns = array(
            0 =>'author_first_name',
            1 =>'author_dob',
            2=> 'author_nationality',
            3=> 'author_meta',
            4=> 'author_status',
            5=> 'created_at',
            6=> 'created_at',
        );

            $totalData = Author::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $author = Author::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
            else {
            $search = $request->input('search.value');

            $author =  Author::where('author_first_name','LIKE',"%{$search}%")
                        ->orWhere('author_last_name', 'LIKE',"%{$search}%")
                        ->orWhere('author_status', 'LIKE',"%{$search}%")
                        ->orWhere('author_nationality', 'LIKE',"%{$search}%")
                        ->orWhere('author_meta', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = Author::where('author_first_name','LIKE',"%{$search}%")
                            ->orWhere('author_last_name', 'LIKE',"%{$search}%")
                            ->orWhere('author_status', 'LIKE',"%{$search}%")
                            ->orWhere('author_nationality', 'LIKE',"%{$search}%")
                            ->orWhere('author_meta', 'LIKE',"%{$search}%")
                        ->count();
            }

            $data = array();
            if(!empty($author))
            {
            foreach ($author as $value)
            {
            $edit =  url('/admin/author/edit',$value->id);

            $nestedData['name'] = $value->author_first_name.''.$value->author_last_name;
            $nestedData['dob'] = format_date($value->author_dob);
            $nestedData['nationality'] = $value->author_dob;
            $nestedData['meta'] =$value->author_meta;
            $nestedData['status'] =$value->author_status;
            $nestedData['created_at'] =  format_date_time($value->created_at);
            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="'.$edit.'"><i class="fas fa-edit"></i>  Edit</a>
              <a class="dropdown-item"onClick="deleterow('.$value->id.')"><i class="fas fa-trash"></i> Delete</a>
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        return view('author.add');
    }
    public function addProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'author_first_name' => 'required',
            'author_last_name' =>   'required',
        ],
        [
            'author_first_name.required' => 'First Name is Required',
            'author_last_name.required' => 'Last Name is Required',
         ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            $author= new Author();
            $author->author_first_name=$request->author_first_name;
            $author->author_last_name=$request->author_last_name;
            $author->author_dob=db_format_date($request->author_dob);
            $author->author_death=db_format_date($request->author_death);
            $author->author_death_text=$request->author_death_text;

            $author->author_reference_link=$request->author_reference_link;
            $author->author_nationality=$request->author_nationality;
            $author->author_meta=$request->author_meta;
            $author->author_meta_data=$request->author_meta_data;
            $author->author_professions=$request->author_professions;
            $author->author_description=$request->author_description;
            $author->author_status=$request->author_status;

            if ($request->hasFile('author_image')) {
                $fileName = time().'_'.$request->author_image->getClientOriginalName();
                $filePath = $request->file('author_image')->storeAs('uploads/author', $fileName, 'public');
                $author->author_image = $filePath;
            }
            $author->save();
            return response()->json(['code'=>200,'message'=>'Record Added Successfully']);


        }
    }
    public function edit($id)
    {
        $author=Author::where('id','=',$id)->first();
        if($author)
        {
            return view('author.edit',compact('author'));
        }
        return redirect()->back();
    }
    public function editProcess(Request $request)
    {
        $author=Author::where('id','=',$request->id)->first();
        if($author)
        {
            $validator = Validator::make($request->all(), [
                'author_first_name' => 'required',
                'author_last_name' =>   'required',
            ],
            [
                'author_first_name.required' => 'First Name is Required',
                'author_last_name.required' => 'Last Name is Required',
             ]);
            if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            else
            {
                $author->author_first_name=$request->author_first_name;
                $author->author_last_name=$request->author_last_name;
                $author->author_dob=db_format_date($request->author_dob);
                $author->author_death=db_format_date($request->author_death);
                $author->author_death_text=$request->author_death_text;

                $author->author_reference_link=$request->author_reference_link;
                $author->author_nationality=$request->author_nationality;
                $author->author_meta=$request->author_meta;
                $author->author_meta_data=$request->author_meta_data;
                $author->author_professions=$request->author_professions;
                $author->author_description=$request->author_description;
                $author->author_status=$request->author_status;

                if ($request->hasFile('author_image')) {
                    File::delete(public_path('storage/'.$request->author_old_image));
                    $fileName = time().'_'.$request->author_image->getClientOriginalName();
                    $filePath = $request->file('author_image')->storeAs('uploads/author', $fileName, 'public');
                    $author->author_image = $filePath;
                }
                $author->save();
                return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);


            }
        }
    }
    public function delete($id)
    {
        $author=Author::where('id','=',$id)->first();
        if($author)
        {
            $author->delete();
            return response()->json(['code'=>404,'message'=>'Author Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }
}
