<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Category;
use App\Models\Admin\Author;
use App\Models\Admin\Quote;
use App\Models\Admin\QuotoTemplate;
use App\Models\Admin\SubCategory;
use Validator;
use File;
class QuoteController extends Controller
{
    public function index()
    {
        return view('quotes.list');
    }
    public function display(Request $request)
    {
        $columns = array(
            0 =>'id',
            1 =>'id',
            2=> 'author_id',
            3=> 'created_at',
            4=> 'id',
        );

            $totalData = Quote::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $quote = Quote::select('quotes.*','author.author_first_name','author.author_last_name')
                    ->leftjoin('author','author.id','=','quotes.author_id')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
            else {
            $search = $request->input('search.value');

            $quote =  QuotoTemplate::where('template_name','LIKE',"%{$search}%")
                        ->orWhere('template_text_size', 'LIKE',"%{$search}%")
                        ->orWhere('template_status', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            $totalFiltered = QuotoTemplate::where('template_name','LIKE',"%{$search}%")
                            ->orWhere('template_text_size', 'LIKE',"%{$search}%")
                            ->orWhere('template_status', 'LIKE',"%{$search}%")

                        ->count();
            }

            $data = array();
            $category_name= array();
            $sub_category_name= array();
            if(!empty($quote))
            {

            foreach ($quote as $value)
            {
                if(isset($value->quote_category))
                {
                    $category_id = explode(',',$value->quote_category);
                    $category=Category::whereIn('category_id',$category_id)->get();
                    foreach( $category as $values)
                    {
                        $category_name[]=$values->category_name;
                    }
                }
                if(isset($value->quote_sub_category))
                {
                    $subcategory_id = explode(',',$value->quote_sub_category);
                    $subcategory=SubCategory::whereIn('id',$subcategory_id)->get();
                    foreach($subcategory as $sub)
                    {
                        $sub_category_name[]=$sub->subcategory_name;
                    }
                }


            $edit =  url('/admin/quotes/edit',$value->id);

            $nestedData['category'] = $category_name;
            $nestedData['subcategory'] = $sub_category_name;
            $nestedData['author'] =  $value->author_first_name .' '. $value->author_last_name ;
            $nestedData['created_at'] = format_date_time($value->created_at);
            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="'.$edit.'"><i class="fas fa-edit"></i>  Edit</a>
              <a class="dropdown-item"onClick="deleterow('.$value->id.')"><i class="fas fa-trash"></i> Delete</a>
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        $category=Category::where('category_id','>',0)->get();
        $author=Author::where('id','>',0)->get();
        $quoto_template=QuotoTemplate::where('id','>',0)->get();

        return view('quotes.add',compact('category','author','quoto_template'));
    }
    public function addProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'quote_category' => 'required',
        ],
        [
            'quote_category.required' => 'Category is Required',
         ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            if(!empty($request->quote_category))
            {
                $category=implode(',',$request->quote_category);
            }
            else
            {
                $category='';
            }
            if(!empty($request->quote_sub_category))
            {
                $sub_category=implode(',',$request->quote_sub_category);
            }
            else
            {
                $sub_category='';
            }

            $quote= new Quote();
            $quote->quote_category=$category;
            $quote->quote_sub_category=$sub_category;
            $quote->author_id=$request->author_id;
            $quote->quote_text=$request->quote_text;
            $quote->template_id=$request->template_id;
            $quote->quote_has_image=$request->quote_has_image;
            $quote->quotes_status=$request->quotes_status;
            if(isset($request->quote_has_image) && $request->quote_has_image==1)
            {
                if ($request->hasFile('quote_image')) {
                    $fileName = time().'_'.$request->quote_image->getClientOriginalName();
                    $filePath = $request->file('quote_image')->storeAs('uploads/quote_image', $fileName, 'public');
                    $quote->quote_image = $filePath;
                }
            }
            $quote->save();
            return response()->json(['code'=>200,'message'=>'Record Added Successfully']);
        }
    }
    public function edit($id)
    {
        $quote=Quote::where('id','=',$id)->first();
        if($quote)
        {
            $category=Category::where('category_id','>',0)->get();
            $sub_category=SubCategory::where('id','>',0)->get();
            $author=Author::where('id','>',0)->get();
            $quoto_template=QuotoTemplate::where('id','>',0)->get();

            return view('quotes.edit',compact('quote','category','author','quoto_template','sub_category'));
        }
        return redirect()->back();
    }
    public function editProcess(Request $request)
    {
        $quote=Quote::where('id','=',$request->id)->first();
        if($quote)
        {
            $validator = Validator::make($request->all(), [
                'quote_category' => 'required',
            ],
            [
                'quote_category.required' => 'Category is Required',
             ]);
            if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            else
            {
                if(!empty($request->quote_category))
                {
                    $category=implode(',',$request->quote_category);
                }
                else
                {
                    $category=Null;
                }
                if(!empty($request->quote_sub_category))
                {
                    $sub_category=implode(',',$request->quote_sub_category);
                }
                else
                {
                    $sub_category=Null;
                }
                $quote->quote_category=$category;
                $quote->quote_sub_category=$sub_category;
                $quote->author_id=$request->author_id;
                $quote->quote_text=$request->quote_text;
                $quote->template_id=$request->template_id;
                $quote->quote_has_image=$request->quote_has_image;
                $quote->quotes_status=$request->quotes_status;
                if(isset($request->quote_has_image) && $request->quote_has_image==1)
                {
                    if ($request->hasFile('quote_image')) {
                        File::delete(public_path('storage/'.$request->old_image));
                        $fileName = time().'_'.$request->quote_image->getClientOriginalName();
                        $filePath = $request->file('quote_image')->storeAs('uploads/quote_image', $fileName, 'public');
                        $quote->quote_image = $filePath;
                    }
                }
                $quote->save();
            return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);
            }
        }
        else
        {
            return redirect('admin/quotes/display');
        }


    }
    public function delete($id)
    {
        $delete=Quote::where('id','=',$id)->first();
        if($delete)
        {
            $delete->delete();
            return response()->json(['code'=>404,'message'=>'Record Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }
}
