<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuotoTemplate extends Model
{
    use SoftDeletes;
    public $table = 'quoto_template';
}
