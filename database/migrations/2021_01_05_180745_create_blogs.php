<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('blog_id');
            $table->text('blog_title')->nullable();
            $table->integer('blog_category_id')->nullable();
            $table->integer('blog_subcategory_id')->nullable();
            $table->integer('blog_author')->nullable();
            $table->text('blog_text')->nullable();
            $table->string('blog_images')->nullable();
            $table->string('blog_vedio')->nullable();
            $table->enum('blog_status',['Active', 'Inactive'])->default('Inactive');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
