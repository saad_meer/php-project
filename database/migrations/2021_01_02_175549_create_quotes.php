<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('quote_category')->nullable();
            $table->string('quote_sub_category')->nullable();
            $table->integer('author_id')->nullable();
            $table->text('quote_text')->nullable();
            $table->integer('template_id')->nullable();
            $table->string('quote_image')->nullable();
            $table->tinyInteger('quote_has_image')->nullable()->default(0);
            $table->enum('quotes_status',['Active', 'Inactive'])->default('Inactive');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
