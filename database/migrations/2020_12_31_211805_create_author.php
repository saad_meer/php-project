<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('author', function (Blueprint $table) {
            $table->increments('id');
            $table->string('author_first_name')->nullable();
            $table->string('author_last_name')->nullable();
            $table->date('author_dob')->nullable();
            $table->date('author_death')->nullable();
            $table->text('author_description')->nullable();
            $table->string('author_reference_link')->nullable();
            $table->text('author_professions')->nullable();
            $table->string('author_meta')->nullable();
            $table->string('author_meta_data')->nullable();
            $table->string('author_nationality')->nullable();
            $table->enum('author_status',['Active', 'Inactive'])->default('Inactive');
            $table->string('author_image')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('author');
    }
}
