<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotoTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quoto_template', function (Blueprint $table) {
            $table->increments('id');
            $table->string('template_name')->nullable();
            $table->string('template_background_color')->nullable();
            $table->string('template_text_color')->nullable();
            $table->string('template_text_style')->nullable();
            $table->string('template_text_size')->nullable();
            $table->string('template_font_family')->nullable();
            $table->string('template_image')->nullable();
            $table->tinyInteger('template_has_image')->nullable()->default(0);
            $table->enum('template_status',['Active', 'Inactive'])->default('Inactive');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quoto_template');
    }
}
